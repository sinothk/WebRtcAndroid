package com.sinothk.webrtc.live.open.sso

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.dds.core.MainActivity
import com.dds.core.consts.Urls
import com.dds.core.socket.IUserState
import com.dds.core.socket.SocketManager
import com.dds.webrtc.App
import com.dds.webrtc.LauncherActivity
import com.sinothk.webrtc.live.R
import kotlinx.android.synthetic.main.s_sso_activity_main.*

class LoginByUserMameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.s_sso_activity_main)
        val userName = intent.getStringExtra("userName") as String
        if (TextUtils.isEmpty(userName)) {
            startActivity(Intent(this, LauncherActivity::class.java))
        } else {
            // 设置用户名
            App.getInstance().username = userName
            // 添加登录回调
            SocketManager.getInstance().addUserStateCallback(object : IUserState {
                override fun userLogin() {
                    startActivity(Intent(this@LoginByUserMameActivity, MainActivity::class.java))
                    finish()
                }

                override fun userLogout() {}
            })
            // 连接socket:登录
            SocketManager.getInstance().connect(Urls.WS, userName, 0)
        }
    }
}